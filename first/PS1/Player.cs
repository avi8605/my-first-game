
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary; // for InputHandler

namespace PS1
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class Player : Microsoft.Xna.Framework.GameComponent
    {
        private Vector2 position;
        public Vector2 Position
        {
            get { return position; }
        }
        public Vector2 velocity;
        public bool hasJumped;
        readonly Vector2 gravity = new Vector2(0, -9.8f);
       // private readonly float moveRate;
        public Rectangle rectangle;
        private IInputHandler input;
        private SpriteBatch spriteBatch;

        public Player(Game game)
            : base(game)
        {

            input = (IInputHandler)game.Services.GetService(typeof(IInputHandler));
            rectangle = new Rectangle((int)position.X, (int)position.Y, 50, 50);
            position = new Vector2(1000.0f, 0f);
            //moveRate = 200.0f;
        }

        public void Load(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            position += velocity;
            rectangle.X = (int)position.X;
            rectangle.Y = (int)position.Y;
            if (input.KeyboardHandler.IsKeyDown(Keys.Right))
                velocity.X = 3f;
            else if (input.KeyboardHandler.IsKeyDown(Keys.Left))
                velocity.X = -3f;
            else
                velocity.X = 0f;
            if (input.KeyboardHandler.IsKeyDown(Keys.Space) && hasJumped == false)
            {
                position.Y -= 20f;
                velocity.Y = -6f;
                hasJumped = true;
            }
            if (hasJumped)
                velocity.Y += 0.15f;
            if (!hasJumped)
                velocity.Y = 0f;
           
            if (position.X > Game.GraphicsDevice.Viewport.Width - 50)
                position.X = Game.GraphicsDevice.Viewport.Width - 50;
            if (position.X < 0)
                position.X = 0;


            
            base.Update(gameTime);
        }
    }
}
