﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using XELibrary; // for InputHandler

namespace PS1
{
    public class Wall1 : Microsoft.Xna.Framework.GameComponent
    {
        private Vector2 position;
        public Vector2 Position
        {
            get { return position; }
        }

        private readonly float moveRate;
        private bool left;
        private SpriteBatch spriteBatch;
        public Rectangle rectangle;

        // Acts according to player position and behaviour

        public Wall1(Game game, Player player)
            : base(game)
        {
            position = new Vector2(600.0f, 200f);
            moveRate = 150.0f;
            left = true;
            rectangle = new Rectangle((int)position.X, (int)position.Y, 100, 70);
            //this.player = player;
        }

        public void Load(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
           
            base.Update(gameTime);
        }
    }
}
