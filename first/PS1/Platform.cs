﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS1
{
    class Platform : Microsoft.Xna.Framework.GameComponent
    {
        public Vector2 Position
        {
            get { return position; }
        }

        private readonly float moveRate;
        private bool left;
        private SpriteBatch spriteBatch;

       // Texture2D texture;
        Vector2 position;
        public Rectangle rectangle;
        public Platform (Game game, Vector2 newPosition)
            : base(game)
        {
            moveRate = 150.0f;
            left = true;
            position = newPosition;
            rectangle = new Rectangle((int)position.X, (int)position.Y, 120, 50);
        }
        public void Load(SpriteBatch spriteBatch)
        {
            this.spriteBatch = spriteBatch;
        }
        public override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            rectangle.X = (int)position.X;
            rectangle.Y = (int)position.Y;
            if (position.X > Game.GraphicsDevice.Viewport.Width - 100)
                left = true;
            if (position.X < 0)
                left = false;
            if (!left)
                position.X += moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;
            else
                position.X -= moveRate * (float)gameTime.ElapsedGameTime.TotalSeconds;

                base.Update(gameTime);
        }

    }
}
