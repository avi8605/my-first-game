using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using XELibrary;
using Microsoft.Xna.Framework.Media;
namespace PS1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Texture2D charactersTexture;
        Texture2D ball;
        Texture2D cloudy;
        Texture2D plat;
        // Actors
        private Player player;
        //private Enemy enemy;
        private Wall1 wall1;
        List<Platform> platforms = new List<Platform>();


        // Time limit
        private double timeRemaining;
        private SpriteFont timeFont;
        private Vector2 timeRemainingPos;

        // Announcements
        String startString;
        String winString;
        String loseString;

        private SpriteFont announceFont;
        private Vector2 announceFontPos;
        private Vector2 announceFontStartOrigin;
        private Vector2 announceFontWinOrigin;
        private Vector2 announceFontLoseOrigin;
        private float announceFontScale;
        private bool gameStart = true;
        private bool gameEnd = false;
        private bool gameEndWin = false;
        private double startTime;
        private double endTime;

        private InputHandler input;
        private int viewportWidth;
        private int viewportHeight;
        private bool falling;
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;

            input = new InputHandler(this);
            Components.Add(input);

            player = new Player(this);
            Components.Add(player);

            wall1 = new Wall1(this, player);
            Components.Add(wall1);

            //enemy = new Enemy(this, player);
            //Components.Add(enemy);

            platforms.Add(new Platform(this, new Vector2(1300, 600)));
            platforms.Add(new Platform(this, new Vector2(300, 500)));
            platforms.Add(new Platform(this, new Vector2(100, 400)));
            platforms.Add(new Platform(this, new Vector2(700, 300)));

            foreach (Platform platform in platforms)
                Components.Add(platform);

                timeRemaining = 20.0;
            timeRemainingPos = new Vector2(50, 50);

            startTime = 2.0;
            endTime = 1.5;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            viewportHeight = graphics.GraphicsDevice.Viewport.Height;
            viewportWidth = graphics.GraphicsDevice.Viewport.Width;

            announceFontPos = new Vector2(viewportWidth / 2, viewportHeight / 2);
            announceFontScale = 0.1f;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Load the texture that holds the actual sprites.
            //charactersTexture = Content.Load<Texture2D>(@"Textures\Characters");
            ball = Content.Load<Texture2D>(@"Textures\Ball");
            cloudy = Content.Load<Texture2D>(@"Textures\cloud");
            // foreach (Platform platform in platforms)
            plat = Content.Load<Texture2D>(@"Textures\platform");
                // Load the font for the time limit
                timeFont = Content.Load<SpriteFont>(@"Fonts\Arial");
            
            // Load announce font
            announceFont = Content.Load<SpriteFont>(@"Fonts\AnnouncementFont");
            startString = Content.Load<String>(@"Strings\Start");
            winString = Content.Load<String>(@"Strings\Win");
            loseString = Content.Load<String>(@"Strings\Lose");

            announceFontStartOrigin = announceFont.MeasureString(startString) / 2.0f;
            announceFontWinOrigin = announceFont.MeasureString(winString) / 2.0f;
            announceFontLoseOrigin = announceFont.MeasureString(loseString) / 2.0f;
            
            player.Load(spriteBatch);
            //enemy.Load(spriteBatch);
            wall1.Load(spriteBatch);
            foreach (Platform platform in platforms)
                platform.Load(spriteBatch);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape))
                this.Exit();

            if (gameStart && startTime > 0.0)
            {
                announceFontScale += 0.5f * (float)gameTime.ElapsedGameTime.TotalSeconds;
                startTime -= gameTime.ElapsedGameTime.TotalSeconds;
            }
            else if (gameEnd && endTime > 0.0)
            {
                announceFontScale += 0.5f * (float)gameTime.ElapsedGameTime.TotalSeconds;
                endTime -= gameTime.ElapsedGameTime.TotalSeconds;
            }
            else if (!gameEnd)
            {
                gameStart = false;

                // Check for collision - end of game
                if (player.Position.Y > 700)
                {
                    gameEnd = true;
                    gameEndWin = false;
                    announceFontScale = 0.1f;
                }
                if (player.rectangle.isOnTopOf(wall1.rectangle))
                {
                    gameEnd = true;
                    gameEndWin = true;
                    announceFontScale = 0.1f;
                }
                falling = true;
                foreach (Platform platform in platforms)
                    if (player.rectangle.isOnTopOf(platform.rectangle))
                     {
                        falling = false;
                        break;
                     }
                if (!falling)
                {
                    player.velocity.Y = 0f;
                    player.hasJumped = false;
                }
                else
                    player.hasJumped = true;
        

                // Check for time limit
                timeRemaining -= gameTime.ElapsedGameTime.TotalSeconds;
                timeRemaining = Math.Round(timeRemaining, 2);

                if (timeRemaining <= 0.0)
                {
                    timeRemaining = 0.0;
                    gameEnd = true;
                    gameEndWin = false;
                    announceFontScale = 0.1f;
                }
            }

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.SkyBlue);

            spriteBatch.Begin();

            if (gameStart)
                spriteBatch.DrawString(announceFont, startString, announceFontPos, Color.White, 0.0f, announceFontStartOrigin, announceFontScale, SpriteEffects.None, 0);
            else if (gameEnd)
                if (gameEndWin)
                    spriteBatch.DrawString(announceFont, winString, announceFontPos, Color.White, 0.0f, announceFontWinOrigin, announceFontScale, SpriteEffects.None, 0);
                else
                    spriteBatch.DrawString(announceFont, loseString, announceFontPos, Color.White, 0.0f, announceFontLoseOrigin, announceFontScale, SpriteEffects.None, 0);
            else
            {
                // Draw player
                spriteBatch.Draw(ball, player.Position, Color.White);
                // Drow wall
                spriteBatch.Draw(cloudy, wall1.Position, Color.White);
                // Drow platform
                foreach (Platform platform in platforms)
                    spriteBatch.Draw(plat, platform.Position, Color.White);
            }

            // Draw time limit
            spriteBatch.DrawString(timeFont, timeRemaining.ToString(), timeRemainingPos, Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
        
    }
}
static class Helper
{
    const int penMargin = 5;
    public static bool isOnTopOf(this Rectangle r1, Rectangle r2)
    {
        return (r1.Bottom >= r2.Top - penMargin &&
            r1.Bottom <= r2.Top + 1 &&
            r1.Right >= r2.Left + 5 &&
            r1.Left <= r2.Right - 5);
    }
}
